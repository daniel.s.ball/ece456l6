def query_yes_no(question, default="yes"):
    """
	______________________________________________________________	
	http://code.activestate.com/recipes/577058/
	Modified by Daniel Ball for python 3 and removal of sys
	______________________________________________________________
	Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        choice = input(question + prompt).lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' (or 'y' or 'n').")
		

def getKey(keyfile):
	key = readFileBytes(keyfile)
	if key == None:
		key = input("Missing or invalid key file. Enter key:").encode('utf-8')
	while len(key) < 8:
		key = input("key must be at least 8 charcters. Enter key:").encode('utf-8')
	return key[:8]

def makeByteArray(message):
	'''
	Returns a bytearray interpretation of the input message. 
	'''
	if isinstance (message, str):
		return bytearray(message, 'utf-8')
	elif isinstance (message, bytes):
		return bytearray(message)
	elif isinstance (message, bytearray):
		return message
	else:
		print(message)
		raise TypeError(type(message),"is not bytes, bytearray, or string")
		
		
def readFileBytes(filename):
	'''
		reads a file as a binary value and returns a bytearray representation of its contents
		will kill your program if you do dumb things
	'''
	try:
		with open(filename, 'rb') as inFile:
			return bytearray(inFile.read())
	except IOError as e:
		return None
