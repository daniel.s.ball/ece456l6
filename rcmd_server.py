'''
server.py

Classes:
IterativeServer
	iterates through clients, handling one at a time

ConcurrentServer
	Handles multiple clients "simultaneously" (forking)
	
'''
########################################################################################################################
# Imports
########################################################################################################################
import subprocess
import socket
from utils import *
import sys
import argparse
import threading
import traceback
import time
import datetime

########################################################################################################################
# Classes
########################################################################################################################
###----------- Server Class -------------###		(this should be abstract ...)
class Server():
	def __init__(self, verbose):
		self.BUFFSIZE = 1024
		self.verbose = verbose
		
	
	def initSocket(self, port):
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.bind(('', port))
		if self.verbose: print("Server bound to ", self.sock.getsockname())
		
			
	def _notifyMessageReceived(self, command, count, delay, address):
		# print request information at server
		template = ("{}: Request received from {}"
					"\n\tCommand: {}"
					"\n\tCount:   {}"
					"\n\tDelay:   {}")
		print(template.format(getTimeString(), address, command, count, delay))
		
		
	def _executeCommand(self, command):
		result = bytes(getTimeString() + ': ', 'utf-8')
		try: result += subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
		except subprocess.CalledProcessError as e: result += e.output
		return result	
		
	def stop(self):
		self.sock.close()
		
###----------- Iterative Server Class -------------###	
class IterativeServer(Server):
	def __init__(self, verbose):
		super().__init__(verbose)
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	
	
	def start(self):	
		self._listenForCommands()
	
	def _prepareMessage(self, message):
		lenChars = bytearray(str(len(message)), 'utf-8')
		lenChars.reverse()
		lenChars.extend(bytearray(10-len(lenChars)))
		for i in range(10):
			message[i] = lenChars[i]
		return message	
		
	
	def _handleCommandMessage(self, command, count, delay, remoteAddr):
		# self.sock.send(historyString, destination)
		executionHistory = bytearray(bytes(10)) # begin with space for count
		executionHistory.extend(self._executeCommand(command))
		for i in range(int(count) - 1):
			time.sleep(float(delay))
			executionHistory.extend(self._executeCommand(command))
		self.sock.sendto(self._prepareMessage(executionHistory), remoteAddr)
		
	def _listenForCommands(self, timeout=180):
		self.sock.settimeout(timeout)	
		while True:
			msg = self.sock.recvfrom(self.BUFFSIZE)
			if msg is not None:
				commandMessage, remoteAddr = msg
				count, delay, command = commandMessage.decode('utf-8').split(',')
				self._handleCommandMessage(command, count, delay, remoteAddr)
			
			
			
###-----------Concurrent Server Class-------------###		
class ConcurrentServer(Server):
	'''
	Portions of this code were modified from
	http://stackoverflow.com/a/23828265/813385,
	particularly since I've never written a multithreaded python application
	See https://docs.python.org/3/library/threading.html for information on threading.Thread()
	'''
	def __init__(self, verbose):
		super().__init__(verbose)
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		
	def start(self, timeout = 300):
		'''		Begins listening (forever!)'''
		self.listenForClients(timeout)	# allow connections to be accepted
	
	def listenForClients(self, timeout):
		'''
		Listens for new clients. When a client connects, spawn a thread for that client.
		By default, client connections will time out and close after 5 minutes of inactivity.
		'''
		print("Listening for Clients ...")
		self.sock.listen(5)
		while True:
			client, address = self.sock.accept()
			print(address, " status = connected")
			client.settimeout(timeout)
			threading.Thread(target = self.listenToClient, args = (client, address) ).start()
			
	def listenToClient(self, client, address):
		# parse incoming message
		count, delay, command = client.recv(self.BUFFSIZE).decode('utf-8').split(',')
		self._notifyMessageReceived(command, count, delay, address)
		self._handleCommandMessage(command, count, delay, client)
		client.close()	#terminate connection to this client
		print(address, " status = closed")

		
	def _handleCommandMessage(self, command, count, delay, client):
		# perform executions and send result of each
		client.sendall(self._executeCommand(command))
		for i in range(int(count) - 1):
			time.sleep(float(delay))
			client.sendall(self._executeCommand(command))	
########################################################################################################################
# Global Functions
########################################################################################################################
def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Simple client for echo server test")
	parser.add_argument("-p", "--server_port", default=17000, type=int, help='server port')
	parser.add_argument("-k", "--socktype", help="UDP or TCP", required=True)
	parser.add_argument("-v", help = "Add (possibly) helpful progress messages", action='store_true')
	return parser.parse_args()

def getTimeString():
	return time.strftime("%H:%M:%S", time.localtime())
########################################################################################################################
# Main
########################################################################################################################
def main():
	# setup
	exitcode = 0
	server = None
	args = getArgs()
	if args.v: print("Verbose mode active")
	
	# run the server
	try:		
		# create client as specified by socktype
		if(args.socktype == 'TCP'):
			if args.v: print("Starting Concurrent Server")
			server 	= ConcurrentServer(args.v)
		else:
			if args.v: print("Starting Iterative Server")
			server 	= IterativeServer(args.v)
				
		# initialize the server
		server.initSocket(args.server_port)
		
		# start the remote command server
		server.start()		
	except Exception as e:
		print("Exception Caught!", e)
		if args.v: traceback.print_tb(e.__traceback__)
	
	finally:
		if server: server.stop()
		print("Exiting command server")
		exit(exitcode)

if __name__ == '__main__':
	main()