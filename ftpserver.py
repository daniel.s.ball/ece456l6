import argparse
from utils import *
import datetime
import socket

class Server():
	'''
	Server for simple file transfer over tcp.
	Once established, server will receive until client closes.
	
	'''
	def __init__(self, sock, serverAddress):
		self.CHUNK_SIZE = 1024
		self.messageHistory = []
		self.sock = sock
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # allow use of waiting sockets
		self.sock.bind(serverAddress)
		print("Server Started, waiting for client connection...")
		
	def establishConnection(self):
		self.sock.listen(5)	              # enable accepting connections
		self.clientConnection, self.clientAddress = self.sock.accept()
		print("Established connection with ", self.clientAddress)
		
	def receiveFile(self):
		filename = input("Enter the destination file name: ")
		data = self.receiveData()
		print("Writing ", len(data), "bytes")
		try:
			if(data):
				with open(filename, 'wb') as outFile:
					outFile.write(data)
				return True
			else:
				return False
		except:
			print("Error writing file")
	
	def sendAbort(self):
		self.clientConnection.send(b'0')
		
	def receiveData(self):
		partial = []
		print('Notifying client of ready state...')
		self.clientConnection.send(b'1')
		print("Receiving data ...")
		while True:			
			data = self.clientConnection.recv(self.CHUNK_SIZE)
			if not data:
				break
			else:
				print("Received chunk")
				partial.append(data)
		return b''.join(partial)
		
	def __enter__():
		pass
		
def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Simple ftp server")
	parser.add_argument("--serverHost", default='', help="The server's i.p. address")
	parser.add_argument("--serverPort", default=17000, type=int, help="The server's port")
	return parser.parse_args()


def main():
	args = getArgs()
	serverAddress = (args.serverHost, args.serverPort)
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		server = Server(s, serverAddress)
		server.establishConnection()
		if query_yes_no("Accept File?"):
			server.receiveFile()
		else:
			server.sendAbort()
	print("Received file, server exiting.")
				
	
if __name__ == '__main__':
	main()