#! \usr\bin\python

import argparse
from utils import *
import socket

class Client():
	'''
	Client for simple file transfer over tcp.
	'''
	def __init__(self, sock, clientAddress):
		self.CHUNK_SIZE = 1024
		self.sock = sock
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # allow use of waiting sockets
		self.sock.bind(clientAddress)
		print("Client started")

	def connectToServer(self, serverAddress):
		self.sock.connect(serverAddress)
	
	def sendFile(self):
		print("Waiting for go-ahead from server ...")
		if self.sock.recv(1) == b'0': # wait for go-ahead from server
			return "Server declined file transfer"
		filename = input('Enter Filename: ')
		try:
			with open(filename, 'rb') as datafile:
				chunk = datafile.read(self.CHUNK_SIZE)
				while chunk:
					print("Sending chunk...")
					self.sendChunk(chunk)
					chunk = datafile.read(self.CHUNK_SIZE)
			return "File transfer complete."
		except IOError as e:
			return "Error opening file " + filename
								
	def sendChunk(self, chunk):
		self.sock.send(chunk)
		
		
def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Simple ftp client")
	parser.add_argument("--serverHost", default="127.0.0.1")
	parser.add_argument("--serverPort", default=17000, type=int)
	parser.add_argument("--localHost", default='', help='')
	parser.add_argument("--localPort", default=16000, type=int)
	return parser.parse_args()


def main():	
	args = getArgs()
	serverAddress = (args.serverHost, args.serverPort)
	clientAddress = (args.localHost, args.localPort)
	try:
		with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:	#handles graceful closing of socket
			client = Client(s, clientAddress)
			client.connectToServer(serverAddress)
			result = client.sendFile()
		print(result)
	except:
		print("Failed to establish connection, exiting.")
				
	
if __name__ == '__main__':
	main()