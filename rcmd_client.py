'''
client.py

Sends a message to the server: execution_count, time_delay(seconds), unix command
Receives execution time and result from server
Displays execution time and result

Reference: http://www.cs.dartmouth.edu/~campbell/cs50/socketprogramming.html
'''

import argparse
from utils import *
import socket
import sys
import traceback


BUFFSIZE = 65536


def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Simple client for echo server test")
	parser.add_argument("-n", "--server_name", default='localhost', help="ip address")
	parser.add_argument("-p", "--server_port", default=17000, type=int, help='server port')
	parser.add_argument("-e", "--execution_count", default=1, type=int, help='interface to listen for responses')
	parser.add_argument("-t", "--time_delay", default=1, type=int, help='delay between executions of command on remote server')
	parser.add_argument("-c", "--command", default='ls', help='command to execute on rcmd server')
	parser.add_argument("-k", "--socktype", help="UDP or TCP", required=True)
	parser.add_argument("-v", help = "Add (possibly) helpful progress messages", action='store_true')
	return parser.parse_args()

def composeMessage(count, delay, command):
	return bytes(str(count) + ',' +
				 str(delay) + ',' +
				 command, 'utf-8')

def receiveUDP(sock, timeout=180 ):
	sock.settimeout(timeout)	
	try:
		aggregate = []
		currentBytes = 0
		msg, remoteAddr = self.sock.recvfrom(BUFFERLIMIT)	#attempt first read
		totalBytes = msg[:10]
		totalBytes.reverse()
		total = int(totalBytes.decode('utf-8'))
		currentBytes = len(msg)
		
		# receive remainder if any
		aggregate.extend(msg[10:])
		while currentBytes < totalBytes:
			msg, remoteAddr = self.sock.recvfrom(BUFFERLIMIT)
			aggregate.extend(msg)
			currentBytes += len(msg)
		
		# print received information
		print(aggregate.decode('utf-8'))
			

	except socket.timeout as e:
		return None


def sendTCP(msg, serverAddress):
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # create socket
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.settimeout(180)	# establish 3 minute timeout.
	sock.connect(serverAddress)
	sock.send(msg)
	return receiveLong(sock)

def sendUDP(msg, serverAddress):
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.sendto(msg, serverAddress)
	receiveUDP(sock)

def receiveLong(sock):
	partial = []
	while True:			
		data = sock.recv(BUFFSIZE)
		if not data:
			break
		else:
			partial.append(data)
	return b''.join(partial)

def main():
	exitcode = 0
	args = getArgs()
	msg = composeMessage(args.execution_count, args.time_delay, args.command)
	if args.v: print("Verbose mode active")
	try:
		sock = None
		# form server address for socket
		serverip = socket.gethostbyname(args.server_name)
		serverAddress = (serverip, args.server_port)
		if args.v: print("Resolved server address: ", serverAddress)
		
		# create client as specified by socktype
		if(args.socktype == 'TCP'):
			result = sendTCP(msg, serverAddress)
		else:
			result = sendUDP(msg, serverAddress)
		print(result.decode('utf-8'))
		
	except socket.gaierror as e:
		print('''ERROR: The server name provided could not be resolved to an ipv4 address.
			  Check that the name is correct and the server is running.''')
		exitcode = 1
	except ConnectionRefusedError as e:
		print('''ERROR: Connection was refused by the server.
			  Check that you have the right address and that the server is running.''')
		exitcode = 2
	except socket.timeout as e:
		print("Timed out while waiting for response from server")
		exitcode = 3
	except Exception as e:
		print("Unhandled Exception: ", e)
		traceback.print_tb(e.__traceback__)
	finally:
		if sock is not None: sock.close()
		print("Exiting rcmd.")
		exit(exitcode)

		

if __name__ == '__main__':
	main()